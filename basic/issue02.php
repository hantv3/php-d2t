<?php
    $oneHundredElementArrayTen = array_fill(0, 100, 10);
    $totalElementArray = count($oneHundredElementArrayTen);
    $newArray = [];
    for ($i = 0; $i < $totalElementArray; $i++) {
        $newArray[$i] = $oneHundredElementArrayTen[$i] + rand(1, 100);
    }
    echo "Sắp xếp theo thứ tự tăng dần";
    echo "<br>";
    sort($newArray);
    for ($i = 0; $i < $totalElementArray; $i++) {
        echo $newArray[$i] . ', ';
    }

    echo "<br>";
    echo "Sắp xếp theo thứ tự giảm dần";
    echo "<br>";
    rsort($newArray);

    for ($i = 0; $i < $totalElementArray; $i++) {
        echo $newArray[$i] . ', ';
    }
?>