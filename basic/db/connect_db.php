<?php
class dbconnection
{
    public function connect() {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $connection = mysqli_connect('localhost', 'master', '43haimot', 'northwind');
        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }
        mysqli_set_charset($connection, 'utf8mb4');
        return $connection;
    }

    public function execute($sql) {
        $connection = $this->connect();
        $stmt = mysqli_query($connection, $sql);
        return $stmt;
    }   

    public function getNumRow() {
        $sql = "SELECT user_id FROM tb_users";
        $result = $this->execute($sql);
        $countNumber = 0;
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $countNumber++;
            }
        } else {
            echo "Kết quả hiện tại là: 0";
        }
        echo "Số lượng bản ghi hiện tại là: " . $countNumber;
        $this->freeresult($result);
    }

    public function getRow() {
        $sql = "SELECT user_id, username, user_email, user_from FROM tb_users";
        $result = $this->execute($sql);
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
              echo "user_id: " . $row["user_id"]. " - username: " 
              . $row["username"]. " - user_email: " . $row["user_email"]
              . " - user_from: " . $row["user_from"]. "<br>";
            }
        } else {
            echo "Kết quả hiện tại là: 0";
        }
        $this->freeresult($result);
    }

    public function freeresult($freeResult) {
        mysqli_free_result($freeResult);
    }

    public function close() {
        $connection = $this->connect();
        mysqli_close($connection);
    }

    public function getNextId() {
        $sql = "SELECT user_id FROM tb_users ORDER BY user_id DESC LIMIT 1";
        $result = $this->execute($sql);
        $nextNumber = mysqli_fetch_assoc($result);
        echo "ID của phần tử tiếp theo là: " . $nextNumber['user_id'] + 1;
    }

    public function createTable() {
        $sql = "CREATE TABLE tb_users (
            user_id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(60) NOT NULL,
            username VARCHAR(25) NOT NULL,
            password VARCHAR(32) NOT NULL,
            user_email VARCHAR(255) NOT NULL,
            femail VARCHAR(255) NOT NULL,
            user_from VARCHAR(100) NULL,
            user_interests VARCHAR(150) NOT NULL,
            user_sig VARCHAR(255) NULL,
            user_viewemail TINYINT(2) NULL,
            user_theme INT(3) NULL,
            user_aim VARCHAR(18) NULL,
            user_yim VARCHAR(25) NULL,
            user_msnm VARCHAR(25) NULL,
            user_password VARCHAR(40) NOT NULL
        )";
        try {
            $result = $this->execute($sql);
            if ($result ) {
                echo "Tạo bảng tb_user thành công";
            }
            $this->freeresult($result);
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }
}
$test = new dbconnection();
$test->createTable();
echo "<br>";
$test->getNumRow();
echo "<br>";
$test->getRow();
echo "<br>";
$test->getNextId();
$test->close();
