<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <style>
        form {
            /* Center the form on the page */
            margin: 0 auto;
            width: 300px;
            /* Form outline */
            padding: 1em;
        }
        ul {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        form li+li {
            margin-top: 1em;
        }

        input,
        textarea {
            /* To make sure that all text fields have the same font settings
     By default, textareas have a monospace font */
            font: 1em sans-serif;

            /* Uniform text field size */
            width: 300px;
            box-sizing: border-box;

            /* Match form field borders */
            border: 1px solid #999;
        }

        input:focus,
        textarea:focus {
            /* Additional highlight for focused elements */
            border-color: #000;
        }

        textarea {
            /* Align multiline text fields with their labels */
            vertical-align: top;

            /* Provide space to type some text */
            height: 5em;
        }

        .button {
            /* Align buttons with the text fields */
            padding-left: 90px;
            /* same size as the label elements */
        }

        button {
            /* This extra margin represent roughly the same space as the space
     between the labels and their text fields */
            margin-left: .5em;
        }
        .text-red {
            color: red;
        }
    </style>
</head>

<body>
    <form action="./handleLogin.php" method="POST">
        <fieldset>
            <label for="username">Tài khoản:</label><br>
            <input type="text" id="username" name="username"><br>
            <br>
            <label for="password">Mật khẩu:</label><br>
            <input type="password" id="password" name="password"><br>
            <br>
            <br>
            <input type="submit" value="Submit">
            <br>
            <?php if (isset($_GET['login-error'])):?>
                <span class="text-red"><?= $_GET['login-error']?></span>
            <?php endif ?>
        </fieldset>
    </form>
</body>

</html>