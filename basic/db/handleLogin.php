<?php
class handleLogin
{
    public function connect() {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $connection = new mysqli('localhost', 'master', '43haimot', 'northwind');
        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }
        mysqli_set_charset($connection, 'utf8mb4');
        return $connection;
    }

    public function execute($sql, $username) {
        $connection = $this->connect();
        $stmt = $connection->prepare($sql);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }
    public function findUser($username, $password) {
        $sql = "SELECT * FROM tb_users WHERE tb_users.username = ?";
        $connection = $this->execute($sql, $username);
        $result = $connection->fetch_assoc();
        $hashPassword = md5($password);
        $this->freeresult($connection);
        if (session_id() === '') session_start();
        
        if ($result['password'] == $hashPassword) {
            echo "user_id: " . $result["user_id"]. " - username: " 
            . $result["username"]. " - user_email: " . $result["user_email"]
            . " - user_from: " . $result["user_from"]. "<br>";
        } else {
            
            $error = "login-error=Vui lòng nhập lại tài khoản hoặc mật khẩu!";
            header('location: form.php?' . $error);
            die;
        }
    }
    public function freeresult($freeResult) {
        mysqli_free_result($freeResult);
    }

    public function close() {
        $connection = $this->connect();
        mysqli_close($connection);
    }
}
$username = $_POST['username'];
$password = $_POST['password'];
$user = new handleLogin();
$user->findUser($username , $password);
$user->close();