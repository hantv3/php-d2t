<?php
include_once '../DTO/user_DTO.php';
include_once '../DAO/user_DAO.php';
class UserBUS
{
    public function checkInputUser($param){
            $errors = "";
            $username = $param->get_name();
            $account = $param->get_account();
            $password = $param->get_password();
            if ($username == '') {
                $errors .= "error-name=Không bỏ trống user name";
            }

            if ($account == '') {
                $errors .= "&error-account=Không bỏ trống user name";
            }

            if ($password == '') {
                $errors .= "&error-password=Không bỏ trống user name";
            }

            if (strlen($errors) > 0) {
                header('location: http://localhost/php-d2t/basic/n-tier/GUI/create.php?' . $errors);
                die;
            }
            return $param;
    }

    public function insertUser($param) {
        $userDao = new UserDAO();
        $user = $this->checkInputUser($param);
        $userDao->insertDaoUser($user);
    }

    public function selectAllUser() {
        $userDao = new UserDAO();
        $result = $userDao->selectDaoUserAll();
        return $result;
    }

    public function deleteUser($param) {
        $userDao = new UserDAO();
        if ($param < 0 || $param == '') {
            $error = 'delete-error';
            header('location: http://localhost/php-d2t/basic/n-tier/GUI/index.php?'. $error);
            die;
        }
        $userDao->deleteDaoUser($param);
    }
}

?>