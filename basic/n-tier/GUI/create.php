<?php
include_once '../BUS/user_BUS.php';
function handleInsertUser($username, $account, $password) {
    $user = new User();
    $user->set_name($username);
    $user->set_account($account);
    $user->set_password($password);
    $userBUS = new UserBUS();
    $userBUS->insertUser($user);
}

if (isset($_POST['submit'])) {
    $username = $_POST['name'];
    $account = $_POST['account'];
    $password = $_POST['password'];
    handleInsertUser($username, $account, $password);
    header('location: index.php');
    die;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <form method="POST">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" placeholder="Enter Name">
                </div>
                <div class="form-group">
                    <label for="account">Account</label>
                    <input type="email" name="account" class="form-control" id="account" aria-describedby="emailHelp" placeholder="Enter Account">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
            </form>
        </div>
    </div>
</body>

</html>