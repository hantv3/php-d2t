<?php
include_once '../BUS/user_BUS.php';
$userBUS = new UserBUS();
$rows = $userBUS->selectAllUser();
if (isset($_POST['idUser'])) {
    $id = $_POST['idUser'];
    $userBUS->deleteUser($id);
    header("Refresh:0");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">ID</th>
                <th scope="col">Tên</th>
                <th scope="col">Email</th>
                <th scope="col">
                <a href="./create.php" type="button" class="btn btn-success">Create</a>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php $ascending = 1; if ($rows): ?>
            <?php foreach($rows as $row): ?>
                <tr>
                    <th scope="row"><?= $ascending++ ?></th>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['username'] ?></td>
                    <td><?= $row['account'] ?></td>
                    <td>
                        <form method="POST">
                            <input type="hidden" name="idUser" value="<?= $row['id']?>">
                            <button type="submit" onclick="myFunction()" class="btn btn-danger btn-sm">Remove</button>
                        </form>
                        <a href="./edit.php" class="btn btn-success btn-sm">Edit</a>
                    </td>
                </tr>
            <?php endforeach ?>
            <?php else:?>
                <h4>No data</h4>
            <?php endif?>
        </tbody>
    </table>
    <script>
    function myFunction() {
        confirm("Do you want delete it!");
    }
</script>
</body>

</html>