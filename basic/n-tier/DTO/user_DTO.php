<?php
class User
{
    private $name;
    private $account;
    private $password;

    public function set_name($name) {
        $this->name = $name;
    }
    public function set_account($account) {
        $this->account = $account;
    }
    public function set_password($password) {
        $this->password = $password;
    }
    
    public function get_name() {
        return $this->name;
    }
    public function get_account() {
        return $this->account;
    }
    public function get_password() {
        return $this->password;
    }
}
?>