<?php
class UserDAO
{
    public function connect() {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $connection = new mysqli('localhost', 'master', '43haimot', 'tts_php');
        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }
        mysqli_set_charset($connection, 'utf8mb4');
        return $connection;
    }

    public function execute() {
        $connection = $this->connect();
        return $connection;
    }
    
    public function insertDaoUser($param) {
        $username = $param->get_name();
        $account = $param->get_account();
        $password = $param->get_password();
        $password = md5($password);
        $connection = $this->execute();
        $sql = "INSERT INTO users (username, account, password) VALUES (?, ?, ?)";
        $stmt = $connection->prepare($sql);
        $stmt->bind_param('sss', $username, $account, $password);
        $stmt->execute();
    }

    public function selectDaoUserAll() {
        $connection = $this->execute();
        $sql = "SELECT id, username, account FROM users";
        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();
        $users = $result->fetch_all(MYSQLI_ASSOC);
        return $users;
    }

    public function deleteDaoUser($param) {
        $connection = $this->execute();
        $sql = "DELETE FROM users WHERE id = ?";
        $stmt = $connection->prepare($sql);
        $stmt->bind_param('s', $param);
        $stmt->execute();
    }
}
?>