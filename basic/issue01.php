<?php
    date_default_timezone_set("Asia/Ho_Chi_Minh");
    $hour = date("h");
    $text = '';
    if ($hour < 12) {
        $text = "morning";
    } elseif ($hour >= 12 && $hour < 18) {
        $text = "afternoon";
    } else {
        $text = "evening";
    }
    echo nl2br("Hello Word!\n");
    echo 'Today is ' . '{' . date("Y-m-d") . '}' . nl2br("\n");
    echo "It's " . '{' . date("h:i:s") . '} ' . 'in the ' . '{' . $text . '}';
?>
