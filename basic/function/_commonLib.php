<?php
$firstNumber = $_POST['firstNumber'];
$secondNumber = intval($_POST['secondNumber']);
$math = $_POST['math'];

// var_dump($math);

$errors = "";
if ($firstNumber == '') {
    $errors .= "firstNumber= Vui lòng nhập một số bất kỳ";
}

if ($secondNumber == '') {
    $errors .= "&secondNumber= Vui lòng nhập một số bất kỳ";
}
if ($secondNumber == 0) {
    $errors .= "&secondNumber= Vui lòng nhập một số khác số 0";
}

if (strlen($errors) > 0) {
    header('location: calculator.php?' . $errors);
    return;
}

function calculate1($firstNumber, $secondNumber, $math)
{
	switch ($math) {
        case '+':
            return $firstNumber + $secondNumber;
            break;
        case '-':
            return $firstNumber - $secondNumber;
            break;
        case '*':
            return $firstNumber * $secondNumber;
            break;
        case '/':
            return $firstNumber / $secondNumber;
            break;
    }
}
$shipFunction = 'calculate1';
function calculate2($firstNumber, $secondNumber, $math, &$OptionalParameter) {
    return $OptionalParameter($firstNumber, $secondNumber, $math);   
}
$stringLength = calculate2($firstNumber, $secondNumber, $math, $shipFunction);
if (strlen($stringLength) > 0) {
    header('location: calculator.php?result='. $stringLength);
    die;
}
// echo  calculate2($firstNumber, $secondNumber, $math, $shipFunction);