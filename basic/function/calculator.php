<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculator</title>
    <style>
        form {
            /* Center the form on the page */
            margin: 0 auto;
            width: 500px;
            /* Form outline */
            padding: 1em;
            border-radius: 1em;
        }

        ul {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        form li+li {
            margin-top: 1em;
        }

        input,
        textarea {
            /* To make sure that all text fields have the same font settings
     By default, textareas have a monospace font */
            font: 1em sans-serif;

            /* Uniform text field size */
            width: 300px;
            box-sizing: border-box;

            /* Match form field borders */
            border: 1px solid #999;
        }

        input:focus,
        textarea:focus {
            /* Additional highlight for focused elements */
            border-color: #000;
        }

        textarea {
            /* Align multiline text fields with their labels */
            vertical-align: top;

            /* Provide space to type some text */
            height: 5em;
        }

        .button {
            /* Align buttons with the text fields */
            padding-left: 90px;
            /* same size as the label elements */
        }

        button {
            /* This extra margin represent roughly the same space as the space
     between the labels and their text fields */
            margin-left: .5em;
        }
        .text-red {
            color: red;
        }
    </style>
</head>

<body>
    <form action="./_commonLib.php" method="POST">
        <fieldset>
            <label for="firstNumber">Số thứ 1:</label><br>
            <input type="number" id="firstNumber" name="firstNumber"><br>
            <?php if (isset($_GET['firstNumber'])):?>
                <span class="text-red"><?= $_GET['firstNumber']?></span>
            <?php endif ?>
            <br>
            <label for="secondNumber">Số thứ 2:</label><br>
            <input type="number" id="secondNumber" name="secondNumber"><br>
            <?php if (isset($_GET['secondNumber'])):?>
                <span class="text-red"><?= $_GET['secondNumber']?></span>
            <?php endif ?>
            <br>
            <label for="math">Phép tính:</label><br>
            <select name="math" id="math">
                <option value="+">+</option>
                <option value="-">-</option>
                <option value="*">*</option>
                <option value="/">/</option>
            </select>
            <br>
            <br>
            <input type="submit" value="Submit">
            <br>
            <?php
                if (isset($_GET['result'])) {
                    $result = $_GET['result'];
                    echo "<p>Kết quả: $result</p>";
                }
            ?>
        </fieldset>
    </form>
</body>

</html>