<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .text-red {
            color: red;
        }
    </style>
</head>

<body>  
    <form action="./handleIssue06.php" method="GET">
        <label for="text">Nhập ký tự:</label>
        <input type="text" id="text" name="text">
        <?php if (isset($_GET['text-error'])):?>
            <span class="text-red"><?= $_GET['text-error']?></span>
        <?php endif ?>
        <br>
        <label for="linesNumber">Nhập số dòng in:</label>
        <input type="number" id="linesNumber" name="linesNumber">
        <?php if (isset($_GET['lineNumber-error'])):?>
            <span class="text-red"><?= $_GET['lineNumber-error']?></span>
        <?php endif ?>
        <br><br>
        <input type="submit" value="Submit">
    </form>
    <div class="">
        <?php
            if (isset($_GET['text']) > 0 && isset($_GET['line']) > 0) {
                $linesNumber = $_GET['line'];
                $text = $_GET['text'];
                for ($i = 0; $i <= $linesNumber; $i++) {
                    echo str_repeat($text, $i);
                    echo "<br>";
                }
            }
        ?>  
    </div>
</body>

</html>