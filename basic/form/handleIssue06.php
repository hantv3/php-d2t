<?php
    $errors= "";
    if (isset($_GET['text'])) {
        $text = $_GET['text'];
    } else {
        $text = '';
    }

    if (isset($_GET['text'])) {
        $linesNumber = $_GET['linesNumber'];
    } else {
        $linesNumber = '';
        return;
    }
    
    if (strlen($text) < 0 || $text == '') {
        $errors .= "text-error=*Vui lòng nhập ít nhất một ký tự";
    }

    if ($linesNumber < 0 || $linesNumber == '') {
        $errors .= "&lineNumber-error=*Số bạn nhập không hợp lệ";
    } elseif ($linesNumber > 100) {
        $errors .= "&lineNumber-error=*Vui lòng nhập số dương và <= 100";
    }

    if (strlen($errors) > 0) {
        header('location: issue06.php?'.$errors);
        die;
    }

    if (strlen($text) > 0 && strlen($linesNumber) > 0) {
        header('location: issue06.php?text='. $text . '&line=' . $linesNumber);
        die;
    }
?>