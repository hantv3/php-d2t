<?php
class handleLogin
{
    public function connect() {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $connection = new mysqli('localhost', 'master', '43haimot', 'northwind');
        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }
        mysqli_set_charset($connection, 'utf8mb4');
        return $connection;
    }

    public function execute($sql, $username) {
        $connection = $this->connect();
        $stmt = $connection->prepare($sql);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }
    public function findUser($username, $password) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $sql = "SELECT * FROM tb_users WHERE username = ?";
        $connection = $this->execute($sql, $username);
        $result = $connection->fetch_assoc();
        $hashPassword = md5($password);
        $this->freeresult($connection);
        if (session_id() === '') session_start();
        $errors = "";
        if ($username == '') {
            $errors .= "username-errors= Vui lòng nhập tài khoản!";
        }
        if ($password == '') {
            $errors .= "&password-errors= Vui lòng nhập mật khẩu!";
        }
        if ($result['password'] == $hashPassword) {
            if (!isset($_SESSION['login'])) {
                $arrayCountAndDay =['count' => 1, 'day' => date('d'), ];
                $_SESSION['login'] = $arrayCountAndDay;
                $_SESSION['username'] = $result['username'];
                header('location: informationUser.php');
            }
            if (isset($_SESSION['login'])) {
                $dayOld = $_SESSION['login']['day'];
                $today = date('d');
                if ($dayOld != $today){
                    $arrayCountAndDay =['count' => 1, 'day' => date('d'), ];
                    $_SESSION['login'] = $arrayCountAndDay;
                    $_SESSION['username'] = $result['username'];
                    header('location: informationUser.php');
                    die;
                }
                $_SESSION['login']['count'] += 1;
                $_SESSION['username'] = $result['username'];
                header('location: informationUser.php');
            }
        } else {
            $errors .= "&login-errors=Tài khoản hoặc mật khẩu chưa ký tự đăc biệt!";
        }
        if (strlen($errors) > 0) {
            header('location: form.php?' . $errors);
            die;
        }
    }
    public function freeresult($freeResult) {
        mysqli_free_result($freeResult);
    }

    public function close() {
        $connection = $this->connect();
        mysqli_close($connection);
    }
}
$username = $_POST['username'];
$password = $_POST['password'];
$user = new handleLogin();
$user->findUser($username , $password);
$user->close();