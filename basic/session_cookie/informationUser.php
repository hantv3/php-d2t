<?php
    session_start();
    function logout() {
        header('location: form.php');
        die;
    }
    if (isset($_POST['logout'])) {
        logout();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>
        <p>Xin chào, <?= $_SESSION["username"] ?></p>
        <p>Đây là lần đăng nhập thứ  <?= $_SESSION["login"]['count'] ?> của bạn trong ngày</p>
        <form method="POST">
            <input type="submit" name="logout" value="logout">
        </form>
    </div>
</body>
</html>